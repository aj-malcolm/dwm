/* See LICENSE file for copyright and license details. */

#define TERMINAL "alacritty"

/* appearance */
static const unsigned int borderpx  = 4;        /* border pixel of windows */
static const unsigned int gappih    = 5;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 5;       /* vert inner gap between windows */
static const unsigned int gappoh    = 5;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 5;       /* vert outer gap between windows and screen edge */
static const int showbar            = 1;        /* 0 means no bar */
static const int showsystray        = 1;     /* 0 means no systray */
static const int smartgaps          = 1;        /* 1 means no outer gap when there is only one window */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 1;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int topbar             = 1;        /* 0 means bottom bar */

static const char *fonts[]          = { "Liberation Sans:size=11", "Symbols Nerd Font:size=12", "Noto Color Emoji:pixelsize=10:antialias=true:autohint=true" };
static const char dmenufont[]       = "Fira Sans:size=11";
static char normbgcolor[]           = "#2e2e2e";
static char normbordercolor[]       = "#565656";
static char normfgcolor[]           = "#fffbf6";
static char selbgcolor[]            = "#49a4f8";
static char selbordercolor[]        = "#47a0f3";
static char selfgcolor[]            = "#2e2e2e";
static char titlebgcolor[]          = "#565656";
static char titlebordercolor[]      = "#2e2e2e";
static char titlefgcolor[]          = "#ffffff";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
       [SchemeTitle]  = { titlefgcolor,  titlebgcolor,  titlebordercolor  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

/* layout(s) */
static float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[1]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "1[TT]",      bstack },
	{ "===",      bstackhoriz },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-a", "-m", dmenumon, NULL }; /*, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };*/
static const char *termcmd[]  = { TERMINAL, NULL };

/*First arg only serves to match against key in rules*/
static const char *scratchpadcmd[][9] = {
        {"s", TERMINAL, "-t", "scratchpad", NULL, NULL, NULL},
        {"m", TERMINAL, "-t", "music", "-e", "ncmpcpp", NULL},
        {"t", TERMINAL, "-t", "tasks", "-e", "vit", NULL},
        {"w", TERMINAL, "-t", "vimwiki", "-e", "vim", "-c", "VimwikiIndex", NULL},
        /* {"w", "gvim", "-c", "set titlestring=vimwiki", "-c", "VimwikiIndex", NULL}, */
};


#include <X11/XF86keysym.h>
static Key keys[] = {
	/* modifier                     key                          function        argument */
	{ MODKEY,                       XK_r,                        spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_p,                        spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return,                   spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_w,                        spawn,          SHCMD("firefox") },
	{ MODKEY|ShiftMask,             XK_w,                        spawn,          SHCMD("todo") },
	{ MODKEY,                       XK_e,                        spawn,          SHCMD(TERMINAL " -e ranger") },
	{ MODKEY|ShiftMask,             XK_e,                        spawn,          SHCMD("nemo") },
	{ MODKEY,                       XK_a,                        spawn,          SHCMD("open_pdf") },
	{ MODKEY,                       XK_d,                        spawn,          SHCMD("shortcuts") },
	{ MODKEY,                       XK_m,                        spawn,          SHCMD("dex ~/.local/share/applications/spotify.desktop") },
	{ MODKEY|ShiftMask,             XK_m,                        spawn,          SHCMD("mpdmenu") },
	{ MODKEY|ShiftMask,             XK_BackSpace,                spawn,          SHCMD("toggle_touch") },
	{ MODKEY,                       XK_BackSpace,                spawn,          SHCMD("sysact") },
	{ 0,                            XK_Print,                    spawn,          SHCMD("screenshot") },
	{ ShiftMask,                    XK_Print,                    spawn,          SHCMD("screenshot -a") },
	{ MODKEY,                       XK_grave,                    togglescratch,  {.v = &scratchpadcmd[0] } },
	{ MODKEY|ShiftMask,             XK_grave,                    togglescratch,  {.v = &scratchpadcmd[1] } },
	{ MODKEY,                       XK_F2,                       togglescratch,  {.v = &scratchpadcmd[2] } },
	{ MODKEY,                       XK_F1,                       togglescratch,  {.v = &scratchpadcmd[3] } },

	{ MODKEY,                       XK_s,                        togglesticky,   {0} },
	{ MODKEY,                       XK_b,                        togglebar,      {0} },
	{ MODKEY,                       XK_j,                        focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,                        focusstack,     {.i = -1 } },
 	{ MODKEY|ShiftMask,             XK_j,                        movestack,      {.i = +1 } },
 	{ MODKEY|ShiftMask,             XK_k,                        movestack,      {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_equal,                    incnmaster,     {.i = +1 } },
	{ MODKEY|Mod1Mask,              XK_minus,                    incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,                        setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,                        setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_space,                    zoom,           {0} },
	{ MODKEY,                       XK_Tab,                      view,           {0} },
	{ MODKEY,                       XK_q,                        killclient,     {0} },
        /* Side Stack */
	{ MODKEY,                       XK_t,                        setlayout,      {.v = &layouts[0]} },
        /* Floating */
	{ MODKEY|ShiftMask,             XK_f,                        setlayout,      {.v = &layouts[1]} },
        /* Monocle */
	{ MODKEY,                       XK_f,                        setlayout,      {.v = &layouts[2]} },
        /* Bottom Stack */
	{ MODKEY|ShiftMask,             XK_t,                        setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ShiftMask,             XK_Tab,                      setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,                    togglefloating, {0} },
	{ MODKEY,                       XK_0,                        view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,                        tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_bracketleft,              focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_bracketright,             focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_bracketleft,              tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_bracketright,             tagmon,         {.i = +1 } },

	{ MODKEY|ControlMask,           XK_0,                        defaultgaps,    {0} },
	{ MODKEY|Mod1Mask,              XK_0,                        togglegaps,     {0} },
	{ MODKEY,                       XK_o,                        incrgaps,       {.i = +5 } },
	{ MODKEY,                       XK_i,                        incrgaps,       {.i = -5 } },
	{ MODKEY|ShiftMask,             XK_o,                        incrohgaps,     {.i = +5 } },
	{ MODKEY|ShiftMask,             XK_i,                        incrohgaps,     {.i = -5 } },
	{ MODKEY|ControlMask,           XK_o,                        incrovgaps,     {.i = +5 } },
	{ MODKEY|ControlMask,           XK_i,                        incrovgaps,     {.i = -5 } },
	{ MODKEY|Mod1Mask,              XK_o,                        incrogaps,      {.i = +5 } },
	{ MODKEY|Mod1Mask,              XK_i,                        incrogaps,      {.i = -5 } },

	{ MODKEY|Mod1Mask,              XK_p,                        incrigaps,      {.i = +5 } },
	{ MODKEY|Mod1Mask,              XK_u,                        incrigaps,      {.i = -5 } },
	{ MODKEY|ShiftMask,             XK_p,                        incrihgaps,     {.i = +5 } },
	{ MODKEY|ShiftMask,             XK_u,                        incrihgaps,     {.i = -5 } },
	{ MODKEY|ControlMask,           XK_p,                        incrivgaps,     {.i = +5 } },
	{ MODKEY|ControlMask,           XK_u,                        incrivgaps,     {.i = -5 } },

	{ 0,                            XF86XK_AudioMute,	     spawn,         SHCMD("vol mute") },
	{ 0,                            XF86XK_AudioRaiseVolume,     spawn,         SHCMD("vol up") },
	{ 0,                            XF86XK_AudioLowerVolume,     spawn,         SHCMD("vol down") },
	{ MODKEY|ControlMask,           XK_BackSpace,	             spawn,         SHCMD("vol mute") },
	{ MODKEY|ControlMask,           XK_equal,        	     spawn,         SHCMD("vol up") },
	{ MODKEY|ControlMask,           XK_minus,        	     spawn,         SHCMD("vol down") },

	{ 0,                            XF86XK_AudioPrev,	     spawn,         SHCMD("music_display -p") },
	{ 0,                            XF86XK_AudioNext,	     spawn,         SHCMD("music_display -n") },
	{ 0,                            XF86XK_AudioPlay,	     spawn,         SHCMD("music_display -t") },

	{ 0,                            XF86XK_MonBrightnessUp,	     spawn,         SHCMD("brightness up") },
	{ 0,                            XF86XK_MonBrightnessDown,    spawn,         SHCMD("brightness down") },
	{ MODKEY|ShiftMask,             XK_equal,        	     spawn,         SHCMD("brightness up") },
	{ MODKEY|ShiftMask,             XK_minus,        	     spawn,         SHCMD("brightness down") },

	{ MODKEY|ControlMask,           XK_k,                        moveresize,     {.v = "0 -0.05 0 0" } },
	{ MODKEY|ControlMask,           XK_j,                        moveresize,     {.v = "0 0.05 0 0" } },
	{ MODKEY|ControlMask,           XK_h,                        moveresize,     {.v = "-0.05 0 0 0" } },
	{ MODKEY|ControlMask,           XK_l,                        moveresize,     {.v = "0.05 0 0 0" } },
	{ MODKEY|ControlMask|ShiftMask, XK_k,                        moveresize,     {.v = "0 0 0 0.05" } },
	{ MODKEY|ControlMask|ShiftMask, XK_j,                        moveresize,     {.v = "0 0 0 -0.05" } },
	{ MODKEY|ControlMask|ShiftMask, XK_h,                        moveresize,     {.v = "0 0 -0.05 0" } },
	{ MODKEY|ControlMask|ShiftMask, XK_l,                        moveresize,     {.v = "0 0 0.05 0" } },
	{ MODKEY,                       XK_Up,                       moveresizeedge, {.v = "t"} },
	{ MODKEY,                       XK_Down,                     moveresizeedge, {.v = "b"} },
	{ MODKEY,                       XK_Left,                     moveresizeedge, {.v = "l"} },
	{ MODKEY,                       XK_Right,                    moveresizeedge, {.v = "r"} },
	/* { MODKEY|ControlMask|ShiftMask, XK_Up,     moveresizeedge, {.v = "T"} }, */
	/* { MODKEY|ControlMask|ShiftMask, XK_Down,   moveresizeedge, {.v = "B"} }, */
	/* { MODKEY|ControlMask|ShiftMask, XK_Left,   moveresizeedge, {.v = "L"} }, */
	/* { MODKEY|ControlMask|ShiftMask, XK_Right,  moveresizeedge, {.v = "R"} }, */

        /* quit{0} kills dwm, quit{1} restarts it */
	{ MODKEY|ShiftMask,     XK_c,         quit,           {0} },
	{ MODKEY|ShiftMask,     XK_q,         quit,           {1} },

	TAGKEYS(                XK_1,                        0)
	TAGKEYS(                XK_2,                        1)
	TAGKEYS(                XK_3,                        2)
	TAGKEYS(                XK_4,                        3)
	TAGKEYS(                XK_5,                        4)
	TAGKEYS(                XK_6,                        5)
	TAGKEYS(                XK_7,                        6)
	TAGKEYS(                XK_8,                        7)
	TAGKEYS(                XK_9,                        8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
		{ "normbgcolor",        STRING,  &normbgcolor },
		{ "normbordercolor",    STRING,  &normbordercolor },
		{ "normfgcolor",        STRING,  &normfgcolor },
		{ "selbgcolor",         STRING,  &selbgcolor },
		{ "selbordercolor",     STRING,  &selbordercolor },
		{ "selfgcolor",         STRING,  &selfgcolor },
		/* { "borderpx",          	INTEGER, &borderpx }, */
		/* { "snap",          	INTEGER, &snap }, */
		/* { "showbar",          	INTEGER, &showbar }, */
		/* { "topbar",          	INTEGER, &topbar }, */
		{ "nmaster",          	INTEGER, &nmaster },
		{ "resizehints",       	INTEGER, &resizehints },
		{ "mfact",      	FLOAT,   &mfact },
};


static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
         *
         * Tags mask:
         *  1 << 8 -- left shift 1 by 8 positions, generating "9"
	 *
         * Float positions:
         *     x -> left = 0, y -> top = 0 
         */

	/* class               instance        title           tags mask  isfloating   monitor  float x,y,  w,h     scratch key */
	{ "Gimp",              NULL,           NULL,           0,         1,           -1,      0.5,0.5,    -1, -1,   0  },
	{ "qBittorrent",       NULL,           NULL,           0,         1,           -1,      0.5,0.5,   0.8,0.8,   0  },
	{ "File-roller",       NULL,           NULL,           0,         1,           -1,      0.5,0.5,   0.8,0.8,   0  },
	{ NULL,                NULL,           "scratchpad",   0,         1,           -1,      0.5,0.5,   0.6,0.6,   's' },
	{ NULL,                NULL,           "music",        0,         1,           -1,      0.5,0.5,   0.6,0.6,   'm' },
	{ NULL,                NULL,           "tasks",        0,         1,           -1,      0.5,0.5,   0.6,0.6,   't' },
	{ NULL,                NULL,           "vimwiki",      0,         1,           -1,      0.5,0.5,   0.6,0.8,   'w' },
	{ NULL,                NULL,           "popup",        0,         1,           -1,      0.9,0.0,   0.4,0.2,   0 },
	{ NULL,                NULL,   "Microsoft Teams Notification", 0, 1,           -1,      0.9,0.0,   0.4,0.2,   0 },
	{ NULL,                "Msgcompose",   NULL,           0,         1,           -1,      0.5,0.5,    -1, -1,   0 },
	{ "Blueman-manager",   NULL,           NULL,           0,         1,           -1,      0.5,0.5,    -1, -1,   0 },
	{ "Pavucontrol",       NULL,           NULL,           0,         1,           -1,      0.5,0.5,    -1, -1,   0 },
        { "Nm-connection-editor",  NULL,       NULL,           0,         1,           -1,      0.5,0.5,    -1, -1,   0 },	
        { "Arandr",            NULL,           NULL,           0,         1,           -1,      0.5,0.5,    -1, -1,   0 },
	{ "matplotlib",        NULL,           NULL,           0,         1,           -1,      0.5,0.5,    -1, -1,   0 },
	{ "feh",               NULL,           NULL,           0,         1,           -1,      0.5,0.5,    -1, -1,   0 },
	{ "ProtonMail Bridge", NULL,           NULL,           0,         1,           -1,      0.5,0.5,    -1, -1,   0 },
	{ "Barrier",           NULL,           NULL,           0,         1,           -1,      1.0,0.0,    -1, -1,   0 },
	{ "Steam",             NULL,           NULL,           1 << 8,    0,           -1,      0.5,0.5,    -1, -1,   0 },
	{ "Spotify",           NULL,           NULL,           1 << 8,    0,           -1,      0.5,0.5,    -1, -1,   0 },
	{ "Microsoft Teams - Preview",  NULL,  NULL,           1 << 7,    0,           -1,      0.5,0.5,    -1, -1,   0 },
};

